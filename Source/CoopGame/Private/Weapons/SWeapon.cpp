// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapons/SWeapon.h"
#include "Player/SCharacter.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/PrimitiveComponent.h"
#include "GameFramework/Character.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystem.h"
#include "Particles/ParticleSystemComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Sound/SoundCue.h"
#include "Components/DecalComponent.h"
#include "Weapons/SProjectile.h"
#include "Net/UnrealNetwork.h"

// Sets default values
ASWeapon::ASWeapon()
{
	MeshComp = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMeshComponent"));
	RootComponent = MeshComp;

	NetUpdateFrequency = 66.0f;
	MinNetUpdateFrequency = 33.0f;

	//NETWORK. Also check BP for Replicates variable set to true
	//SetReplicates(true); //if we spawn weapon on server, then it will spawn weapon on the clients
	SetReplicates(true);
}

void ASWeapon::BeginPlay()
{
	Super::BeginPlay();

}

void ASWeapon::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

void ASWeapon::MakeShot()
{
	if(CanFire)
	{
		GetWorldTimerManager().SetTimer(HandleFireRateTimer, this, &ASWeapon::Fire, FireRate, true, 0.0f);
		return;
	}
	GetWorldTimerManager().ClearTimer(HandleFireRateTimer);
}

void ASWeapon::ServerFire_Implementation()
{
	Fire();
}

bool ASWeapon::ServerFire_Validate()
{
	return true;
}

void ASWeapon::Fire()
{
	if(GetLocalRole() < ENetRole::ROLE_Authority)
	{
		ServerFire();
	}

	PlayFireEffects();
	PlayAmmoEjects();

	const auto Character = Cast<ACharacter>(GetOwner());
	if (!Character) return;
	const auto Controller = Cast<AController>(Character->GetController());
	if (!Controller) return;

	FVector ViewLocation;
	FRotator ViewRotation;
	FCollisionQueryParams CollisionQueryParams;
	FHitResult HitResult;
		
	Controller->GetPlayerViewPoint(ViewLocation, ViewRotation);  // fill these params sent with data of owning player

	//Bullet spread feature
	const auto HalfRad = FMath::DegreesToRadians(BulletSpread);

	const FVector ShootDirection = FMath::VRandCone(ViewRotation.Vector(), HalfRad);

	FVector TraceEnd = ViewLocation + (ShootDirection * 15000);

	CollisionQueryParams.AddIgnoredActor(GetOwner());
	CollisionQueryParams.bReturnPhysicalMaterial = true;

	if (!GetWorld()) return;
	GetWorld()->LineTraceSingleByChannel(HitResult, ViewLocation, TraceEnd, ECollisionChannel::ECC_Visibility, CollisionQueryParams);

	//Getting Data from linetraced HitResult
	const auto TraceEndPoint = HitResult.ImpactPoint;
	const auto PhysMat = Cast<UPhysicalMaterial>(HitResult.PhysMaterial);
	const auto HitActor = Cast<AActor>(HitResult.GetActor());

	//NETWORK
	if (GetLocalRole() == ENetRole::ROLE_Authority)
	{	//Replicate on Server and go to all the clients
		HitScanTrace.TraceTo = HitResult.ImpactPoint;
		HitScanTrace.PhysMat = Cast<UPhysicalMaterial>(HitResult.PhysMaterial);
	}

	//HEADSHOT or SimpleDamage
	if (HeadShotPhysMat && HeadShotPhysMat == HitResult.PhysMaterial.GetEvenIfUnreachable())
	{
		UGameplayStatics::ApplyPointDamage(HitActor, 100.0f, ShootDirection, HitResult, Controller, Character, DamageTypeClass);
		PlayImpactFX(PhysMat, TraceEndPoint);
	}
	else
	{
		UGameplayStatics::ApplyPointDamage(HitActor, DamageValue * FMath::FRandRange(1.0f, 2.0f), ShootDirection, HitResult, Controller, Character, DamageTypeClass);
		PlayImpactFX(PhysMat, TraceEndPoint);
		if (HitActor)
		{
			UE_LOG(LogTemp, Warning, TEXT("Point damage to: %s"), *HitActor->GetName());
		}
		
	}
}

//NETWORK
void ASWeapon::OnRep_HitScanTrace()
{
	// Play cosmetic FX
	PlayFireEffects();
	PlayImpactFX(HitScanTrace.PhysMat, HitScanTrace.TraceTo);
}

void ASWeapon::PlayFireEffects()
{
	if (MuzzleEffect)
	{
		UGameplayStatics::SpawnEmitterAttached(MuzzleEffect, MeshComp, MuzzleSocketName);
	}

	if (TracerEffect)
	{
		UParticleSystemComponent* TracerComp = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), TracerEffect, MeshComp->GetSocketLocation(MuzzleSocketName));
		if (TracerComp)
		{
			TracerComp->SetVectorParameter(TracerTargetName, HitScanTrace.TraceTo);
		}
	}

	PlayCameraShake();
}

void ASWeapon::PlayCameraShake()
{
	const auto Character = Cast<ASCharacter>(GetOwner());
	if (Character)
	{
		const auto Controller = Cast<APlayerController>(Character->GetController());
		if (Controller)
		{
			Controller->ClientStartCameraShake(FireCameraShake);
		}
	}
}

void ASWeapon::PlayImpactFX(const UPhysicalMaterial* PhysMat, const FVector ImpactPoint)
{
	auto ImpactData = DefaultImpactData;

	if (IsValid(PhysMat))
	{
		if (ImpactDataMap.Contains(PhysMat))
			{
				ImpactData = ImpactDataMap[PhysMat];
			}
	}

	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactData.ParticleEffect, ImpactPoint, ImpactPoint.Rotation());
	
	//decal
	//auto DecalComponent = UGameplayStatics::SpawnDecalAtLocation(GetWorld(),
	//															 ImpactData.DecalData.Material,
	//															 ImpactData.DecalData.Size,
	//															 Hit.ImpactPoint,
	//															 Hit.ImpactNormal.Rotation());

	//if (DecalComponent)
	//{
	//	DecalComponent->SetFadeOut(ImpactData.DecalData.LifeTime, ImpactData.DecalData.FadeOutTime);
	//}

	//sound
	if (ImpactData.Sound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ImpactData.Sound, ImpactPoint);
	}
}

void ASWeapon::PlayAmmoEjects()
{
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	GetWorld()->SpawnActor<ASProjectile>(MeshBulletEjected, MeshComp->GetSocketTransform(AmmoEjectSocketName), SpawnParameters);
}

//NETWORK. Replicate this variable on the Server and replicate it to the client
void ASWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	//MACRO work with #include "Net/UnrealNetwork.h"
	DOREPLIFETIME_CONDITION(ASWeapon, HitScanTrace, COND_SkipOwner);
}
