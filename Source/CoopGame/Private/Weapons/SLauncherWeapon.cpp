// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapons/SLauncherWeapon.h"
#include "Player/SCharacter.h"
#include "Weapons/SProjectile.h"
#include "Kismet/GameplayStatics.h"

void ASLauncherWeapon::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	//if (GetWorldTimerManager().IsTimerActive(HandleFireRateLauncherTimer))
	//{
	//	float secs = GetWorldTimerManager().GetTimerRemaining(HandleFireRateLauncherTimer);
	//	UE_LOG(LogTemp, Display, TEXT("Remains: %f"), secs);
	//}
}

void ASLauncherWeapon::Fire()
{
	// try and fire a projectile
	if (Projectile && CanFire)
	{
		const auto Character = Cast<ACharacter>(GetOwner());
		if (!Character) return;

		const auto Controller = Cast<AController>(Character->GetController());
		if (!Controller) return;

		FVector ViewLocation;
		FRotator ViewRotation;
		Controller->GetPlayerViewPoint(ViewLocation, ViewRotation);  // fill these params sent with data of owning player

		//Set Spawn Collision Handling Override
		FActorSpawnParameters ActorSpawnParams;
		ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

		// spawn the projectile at the muzzle
		GetWorld()->SpawnActor<ASProjectile>(Projectile, MeshComp->GetSocketLocation(MuzzleSocketName), ViewRotation, ActorSpawnParams);
		UGameplayStatics::SpawnEmitterAttached(MuzzleEffect, MeshComp, MuzzleSocketName);

		PlayCameraShake();

		CanFire = false;
		GetWorldTimerManager().SetTimer(HandleFireRateLauncherTimer, this, &ASLauncherWeapon::SetLauncherReadyToShot, FireRate, false, LauncherReloadingTime);
		UE_LOG(LogTemp, Warning, TEXT("Launcher Reloading"));
	}
	
}

void ASLauncherWeapon::MakeShot()
{
	if (!CanFire) return;
	GetWorldTimerManager().SetTimer(HandleFireRateTimer, this, &ASLauncherWeapon::Fire, FireRate, false, 0.0f);
}

void ASLauncherWeapon::SetLauncherReadyToShot()
{
	CanFire = true;
	GetWorldTimerManager().ClearTimer(HandleFireRateTimer);
	UE_LOG(LogTemp, Warning, TEXT("Launcher Ready"));
}