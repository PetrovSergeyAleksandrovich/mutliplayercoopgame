// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/SHealthComponent.h"

#include "SGameMode.h"
#include "Player/SCharacter.h"
#include "Gameframework/Character.h"
#include "Gameframework/Controller.h"
#include "Engine/World.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
USHealthComponent::USHealthComponent()
{
	SetIsReplicatedByDefault(true);
}


// Called when the game starts
void USHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	//NETWORK. Only hook if we are server
	const auto Owner = Cast<AActor>(GetOwner());
	if (Owner)
	{
		if (Owner->GetLocalRole() == ENetRole::ROLE_Authority)
		{
			Owner->OnTakePointDamage.AddDynamic(this, &USHealthComponent::HandleTakePointDamage);
			Owner->OnTakeRadialDamage.AddDynamic(this, &USHealthComponent::HandleTakeRadialDamage);
			//UE_LOG(LogTemp, Error, TEXT("HealthComponent Binding Delegates Done for Character %s"), *GetOwner()->GetName());
		}
	}

	DefaultHealth = Health;
}

void USHealthComponent::HandleTakePointDamage(AActor* DamagedActor,
												   float Damage,
												   class AController* InstigatedBy,
												   FVector HitLocation,
												   class UPrimitiveComponent* FHitComponent,
												   FName BoneName,
												   FVector ShotFromDirection,
												   const class UDamageType* DamageType,
												   AActor* DamageCauser)
{
	if (bIsDead) return;

	Health = FMath::Clamp(Health - Damage, 0.0f, DefaultHealth);
	OnTakeRadialDamageHealthChanged.Broadcast(this, Health); // Your Own Delegate! You broadcat data params as in MACRO declaration (see above)

	if(Health <= 0.0f)
	{
		bIsDead = true;
		auto GM = Cast<ASGameMode>(GetWorld()->GetAuthGameMode());
		if(GM)
		{
			GM->OnActorKilled.Broadcast(GetOwner(), DamageCauser, InstigatedBy);
		}
	}
}

void USHealthComponent::HandleTakeRadialDamage(AActor* DamagedActor,
													float Damage,
													const class UDamageType* DamageType,
													FVector Origin,
													const FHitResult& HitInfo,
													class AController* InstigatedBy,
													AActor* DamageCauser)
{
	if (bIsDead) return;

	Health = FMath::Clamp(Health - Damage, 0.0f, DefaultHealth);
	OnTakeRadialDamageHealthChanged.Broadcast(this, Health); // Your Own Delegate! You broadcat data params as in MACRO declaration (see above)

	if(Health <= 0.0f)
	{
		bIsDead = true;
		auto GM = Cast<ASGameMode>(GetWorld()->GetAuthGameMode());
		if(GM)
		{
			GM->OnActorKilled.Broadcast(GetOwner(), DamageCauser, InstigatedBy);
		}
	}
}

//NETWORK. For setup Health variable; Replicate this variable on the Server and replicate it to the client
void USHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	//MACRO work with #include "Net/UnrealNetwork.h"
	DOREPLIFETIME(USHealthComponent, Health);
}

void USHealthComponent::OnRep_Health()
{
	OnTakePointDamageHealthChanged.Broadcast(this, Health);

	OnTakeRadialDamageHealthChanged.Broadcast(this, Health);
}

void USHealthComponent::Heal(float HealAmount)
{
	if(HealAmount <= 0.0f || Health == 0.0f)
	{
		return;
	}

	Health = FMath::Clamp(Health + HealAmount, 0.0f, DefaultHealth);

	OnTakePointDamageHealthChanged.Broadcast(this, Health);
}

