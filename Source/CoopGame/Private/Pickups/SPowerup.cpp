// Fill out your copyright notice in the Description page of Project Settings.


#include "Pickups/SPowerup.h"

#include "Net/UnrealNetwork.h"

// Sets default values
ASPowerup::ASPowerup()
{
	SetReplicates(true);
}


void ASPowerup::ActivatePowerup(AActor* ActiveFor)
{
	OnActivated(ActiveFor);

	bIsPowerupActive = true;
	OnRep_PowerupActive();

	if (PowerupInterfall >= 0.0f)
	{
		GetWorldTimerManager().SetTimer(PowerupTickTimerHandle, this, &ASPowerup::OnTickPowerup, PowerupInterfall, true);
	}
	else
	{
		OnTickPowerup();
	}
}

void ASPowerup::OnRep_PowerupActive()
{
	OnPowerupStateChanged(bIsPowerupActive);
}

void ASPowerup::OnTickPowerup()
{
	TickProcessed++;

	if(TickProcessed >= TotalNumOfTicks)
	{
		OnExpired();

		bIsPowerupActive = false;
		OnRep_PowerupActive();

		GetWorldTimerManager().ClearTimer(PowerupTickTimerHandle);
	}
}

void ASPowerup::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	//MACRO works with #include "Net/UnrealNetwork.h"
	DOREPLIFETIME(ASPowerup, bIsPowerupActive);
}