// Fill out your copyright notice in the Description page of Project Settings.


#include "Pickups/SPickup.h"
#include "TimerManager.h"
#include "Components/DecalComponent.h"
#include "GameFramework/GameModeBase.h"
#include "Player/SCharacter.h"

// Sets default values
ASPickup::ASPickup()
{
	SphereComp = CreateDefaultSubobject<USphereComponent>("SphereComp");
	SphereComp->SetSphereRadius(75.0f);
	RootComponent = Cast<USceneComponent>(SphereComp);

	/*DecalComp = CreateDefaultSubobject<UDecalComponent>("DecalComp");
	DecalComp->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f));
	DecalComp->DecalSize = FVector(64.0f, 75.0f, 75.0f);
	DecalComp->SetupAttachment(RootComponent);*/

	SetReplicates(true);
}

// Called when the game starts or when spawned
void ASPickup::BeginPlay()
{
	Super::BeginPlay();

	if(GetLocalRole() == ENetRole::ROLE_Authority)
	{
		Respawn();
	}
}

void ASPickup::Respawn()
{
	if (PowerUpPickup == nullptr) return;

	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	PowerupInst = Cast<ASPowerup>(GetWorld()->SpawnActor<ASPowerup>(PowerUpPickup, GetTransform(), SpawnParams));

}

void ASPickup::NotifyActorBeginOverlap(AActor* OtherActor)
{

	Super::NotifyActorBeginOverlap(OtherActor);

	UE_LOG(LogTemp, Warning, TEXT("Pickup overlaped"));

	if (GetLocalRole() == ENetRole::ROLE_Authority && PowerupInst)
	{
		PowerupInst->ActivatePowerup(OtherActor);
		PowerupInst = nullptr;

		GetWorldTimerManager().SetTimer(TimerHandle_RespwnTimer, this, &ASPickup::Respawn, CoolDownDuration);
	}
	
}
