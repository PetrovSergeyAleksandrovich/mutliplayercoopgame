// Fill out your copyright notice in the Description page of Project Settings.


#include "SGameStateBase.h"

#include "Net/UnrealNetwork.h"

void ASGameStateBase::OnRep_WaveState(EWaveState OldState)
{
	WaveStateChanged(WaveState, OldState);

}

void ASGameStateBase::SetWaveState(EWaveState NewState)
{
	if(GetLocalRole() == ENetRole::ROLE_Authority)
	{
		OnRep_WaveState(WaveState);
		WaveState = NewState;
	}
}


//NETWORK. For setup Health variable; Replicate this variable on the Server and replicate it to the client
void ASGameStateBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	//MACRO work with #include "Net/UnrealNetwork.h"
	DOREPLIFETIME(ASGameStateBase, WaveState);
}