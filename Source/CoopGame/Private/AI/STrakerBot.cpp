// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/STrakerBot.h"
#include "NavigationPath.h"
#include "NavigationSystem.h"
#include "DrawDebugHelpers.h"
#include "Components/SkeletalMeshComponent.h"
#include "Engine/Engine.h"
#include "Kismet/GameplayStatics.h"
#include "Player/SCharacter.h"

// Sets default values
ASTrakerBot::ASTrakerBot()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>("MeshComponent");
	RootComponent = MeshComp;
	MeshComp->SetSimulatePhysics(true);
	bCanAffectNavigationGeneration = false;

	HealthComponent = CreateDefaultSubobject<USHealthComponent>("HealthComponent");

	SphereCollisionComp = CreateDefaultSubobject<USphereComponent>("CollisionSphereComponent");
	SphereCollisionComp->SetupAttachment(GetRootComponent());
	SphereCollisionComp->SetSphereRadius(600.0f, true);
	SphereCollisionComp->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	SphereCollisionComp->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Overlap);
}

// Called when the game starts or when spawned
void ASTrakerBot::BeginPlay()
{
	Super::BeginPlay();

	MatInst = MeshComp->CreateAndSetMaterialInstanceDynamicFromMaterial(0, MeshComp->GetMaterial(0));

	HealthComponent->OnTakePointDamageHealthChanged.AddDynamic(this, &ASTrakerBot::OnHealthChangedToPointDamage);
	HealthComponent->OnTakeRadialDamageHealthChanged.AddDynamic(this, &ASTrakerBot::OnHealthChangedToRadialDamage);

	SphereCollisionComp->OnComponentBeginOverlap.AddDynamic(this, &ASTrakerBot::TriggeredExplosion);

	if (GetLocalRole() == ROLE_Authority)
	{
		NextPathPoint = GetNextPathPoint();
	}

}

// Called every frame
void ASTrakerBot::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (GetLocalRole() == ROLE_Authority)
	{
		NextPathPoint = GetNextPathPoint();

		float DistanceToTarget = (GetActorLocation() - NextPathPoint).Size();

		FVector ForceDirection = NextPathPoint - GetActorLocation();
		ForceDirection.Normalize();
		ForceDirection *= MovementForce;

		if(MeshComp->GetCollisionEnabled() != ECollisionEnabled::NoCollision)
		{
			MeshComp->AddForce(ForceDirection, NAME_None, bVelocityChange);
		}
	}
}

FVector ASTrakerBot::GetNextPathPoint()
{
	const auto PlayerPawn = UGameplayStatics::GetPlayerPawn(this, 0);
	const auto Character = Cast<ASCharacter>(PlayerPawn);

	UNavigationPath* NavPath = UNavigationSystemV1::FindPathToActorSynchronously(this, GetActorLocation(), Character);

	if (NavPath && NavPath->PathPoints.Num() > 1)
	{
		return NavPath->PathPoints[1];
	}

	return GetActorLocation();
}

void ASTrakerBot::OnHealthChangedToPointDamage(USHealthComponent* HealthComp, float Health)
{
	if(Health <= 0.0f)
	{
		
		MeshComp->SetVisibility(false);
		MeshComp->SetSimulatePhysics(false);
		MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		SelfDestructFX();

		if(GetLocalRole() == ENetRole::ROLE_Authority)
		{
			TArray<AActor*> IgnoredActors;
			IgnoredActors.Add(this);

			UGameplayStatics::ApplyRadialDamage(this, 10.0f, GetActorLocation(), 400.0f, nullptr,
				IgnoredActors, this, GetInstigatorController(), true);

			SetLifeSpan(2.0f);
		}

	}

	if(MatInst)
	{
		MatInst->SetScalarParameterValue("LastTimeDamageTaken", GetWorld()->TimeSeconds);
	}
}

void ASTrakerBot::OnHealthChangedToRadialDamage(USHealthComponent* HealthComp, float Health)
{
	if (Health <= 0.0f)
	{

		MeshComp->SetVisibility(false);
		MeshComp->SetSimulatePhysics(false);
		MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		SelfDestructFX();

		if(GetLocalRole() == ENetRole::ROLE_Authority)
		{
			TArray<AActor*> IgnoredActors;
			IgnoredActors.Add(this);

			UGameplayStatics::ApplyRadialDamage(this, 100.0f, GetActorLocation(), 400.0f, nullptr,
				IgnoredActors, this, GetInstigatorController(), true);

		}

	}

	if (MatInst)
	{
		MatInst->SetScalarParameterValue("LastTimeDamageTaken", GetWorld()->TimeSeconds);
	}

}

void ASTrakerBot::SelfDestructFX() const
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionFX, GetActorLocation());
	UGameplayStatics::SpawnSoundAtLocation(this, ExplosionSound, GetActorLocation());
}

void ASTrakerBot::TriggeredExplosion(UPrimitiveComponent* OverlappedComponent, 
									 AActor* OtherActor, 
									 UPrimitiveComponent* OtherComp, 
									 int32 OtherBodyIndex, 
									 bool bFromSweep, 
									 const FHitResult& SweepResult)
{

	Super::NotifyActorBeginOverlap(OtherActor);

	const auto Character = Cast<ACharacter>(OtherActor);
	const auto Bot = Cast<ASTrakerBot>(OtherActor);

	if(Character || Bot)
	{
		MeshComp->SetVisibility(false);
		MeshComp->SetSimulatePhysics(false);
		MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		SelfDestructFX();

		if(GetLocalRole() == ENetRole::ROLE_Authority)
		{
			TArray<AActor*> IgnoredActors;
			IgnoredActors.Add(this);

			UGameplayStatics::ApplyRadialDamage(this, 100.0f, GetActorLocation(), 400.0f, nullptr,
				IgnoredActors, this, GetInstigatorController(), true);

			if(Character)
			{
				Character->GetMesh()->AddRadialImpulse(SweepResult.ImpactNormal, 50.0f, 1000.0f, ERadialImpulseFalloff::RIF_Linear, true);
			}
		}
	}

	K2_DestroyActor();
}

void ASTrakerBot::ExplodeBot()
{

	MeshComp->SetVisibility(false);
	MeshComp->SetSimulatePhysics(false);
	MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	SelfDestructFX();

	if (GetLocalRole() == ENetRole::ROLE_Authority)
	{
		MeshComp->SetVisibility(false);
		MeshComp->SetSimulatePhysics(false);
		MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		SelfDestructFX();

		TArray<AActor*> IgnoredActors;
		//IgnoredActors.Add(this);

		UGameplayStatics::ApplyRadialDamage(this, 5.0f, GetActorLocation(), 400.0f, nullptr,
			IgnoredActors, this, GetInstigatorController(), true);
	}
}

