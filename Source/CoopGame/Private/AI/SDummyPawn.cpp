// Fill out your copyright notice in the Description page of Project Settings.

#include "AI/SDummyPawn.h"

// Sets default values
ASDummyPawn::ASDummyPawn()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>("MeshComponent");
	MeshComp->SetupAttachment(GetRootComponent());
	MeshComp->SetSimulatePhysics(true);
	bCanAffectNavigationGeneration = false;

	HealthComponent = CreateDefaultSubobject<USHealthComponent>("HealthComponent");
}

// Called when the game starts or when spawned
void ASDummyPawn::BeginPlay()
{
	Super::BeginPlay();

	check(HealthComponent);

	if (HealthComponent)
	{
		UE_LOG(LogTemp, Display, TEXT("HealthComponent created for DummyActor"));
	}

	HealthComponent->OnTakePointDamageHealthChanged.AddDynamic(this, &ASDummyPawn::OnHealthChangedToPointDamage);

}

void ASDummyPawn::OnHealthChangedToPointDamage(USHealthComponent* HealthComp, float Damage)
{
	UE_LOG(LogTemp, Display, TEXT("HealthComponent PointDamage DummyPawn"));
}


