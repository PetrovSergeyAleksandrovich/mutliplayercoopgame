// Fill out your copyright notice in the Description page of Project Settings.

#include "AI/SDummyActor.h"

ASDummyActor::ASDummyActor()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>("MeshComponent");
	MeshComp->SetupAttachment(GetRootComponent());
	MeshComp->SetSimulatePhysics(true);

	HealthComponent = CreateDefaultSubobject<USHealthComponent>("HealthComponent");

	SetReplicates(true);
}

void ASDummyActor::BeginPlay()
{
	Super::BeginPlay();

	check(HealthComponent);

	if(HealthComponent)
	{
		UE_LOG(LogTemp, Display, TEXT("HealthComponent created for DummyActor"));
	}

	HealthComponent->OnTakePointDamageHealthChanged.AddDynamic(this, &ASDummyActor::OnHealthChangedToPointDamage);
}

void  ASDummyActor::OnHealthChangedToPointDamage(USHealthComponent* HealthComp, float Damage)
{
	UE_LOG(LogTemp, Display, TEXT("HealthComponent PointDamage DummyActor"));
}

