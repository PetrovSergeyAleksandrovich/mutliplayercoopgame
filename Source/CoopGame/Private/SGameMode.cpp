// Fill out your copyright notice in the Description page of Project Settings.

#include "SGameMode.h"
#include "AI/STrakerBot.h"
#include "EngineUtils.h"
#include "SCharacter.h"
#include "SGameStateBase.h"
#include "SPlayerState.h"
#include "Net/UnrealNetwork.h"
#include "Particles/ParticleEventManager.h"

ASGameMode::ASGameMode()
{
	GameStateClass = ASGameStateBase::StaticClass();
	PlayerStateClass = ASPlayerState::StaticClass();

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickInterval = 10.0f;
}

void ASGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	CheckWaveState();

	CheckAnyPlayerAlive();

	RestartDeadPlayer();
}

void ASGameMode::StartPlay()
{
	Super::StartPlay();

	PrepareForNextWave();
}

void ASGameMode::StartWave()
{
	AmountOfBotsToSpawn = 1 * (WaveCount);
	//WaveCount++;

	UE_LOG(LogTemp, Display, TEXT("Bots: %i, Wave: %i"), AmountOfBotsToSpawn, WaveCount);

	GetWorldTimerManager().SetTimer(TimerHandle_BotSpawner, this, &ASGameMode::SpawnBotTimerElapsed, TimeBetweenSpawningBots, true, 0.0f);
}

void ASGameMode::SpawnBotTimerElapsed()
{
	WaveSpawnFinished = false;

	SpawnNewBot();

	AmountOfBotsToSpawn--;

	if (AmountOfBotsToSpawn <= 0)
	{
		EndWave();
		WaveSpawnFinished = true;
	}

	NewWaveState(EWaveState::WaveInProgress);
}

void ASGameMode::EndWave()
{
	GetWorldTimerManager().ClearTimer(TimerHandle_BotSpawner);
	NewWaveState(EWaveState::WaitingToComplete);
}

void ASGameMode::PrepareForNextWave()
{
	GetWorldTimerManager().SetTimer(TimerHandle_NextWaveStart, this, &ASGameMode::StartWave, TimeBetweenWaves, false);
	NewWaveState(EWaveState::WaitingToStart);
}

void ASGameMode::CheckWaveState()
{
	if ( GetWorldTimerManager().IsTimerActive(TimerHandle_NextWaveStart) ) return;

	for (auto It = TActorIterator<ASTrakerBot>(GetWorld()); It; ++It)
	{
		const auto Bot = Cast<ASTrakerBot>(*It);
		if (Bot)
		{
			return;
		}
	}

	if (WaveSpawnFinished)
	{
		NewWaveState(EWaveState::WaveComplete);
		PrepareForNextWave();
	}
}

void ASGameMode::CheckAnyPlayerAlive()
{

	for (auto It = TActorIterator<ASCharacter>(GetWorld()); It; ++It)
	{
		const auto Player = Cast<ASCharacter>(*It);
		if (Player)
		{
			const auto HealthComp = Cast<USHealthComponent>(Player->GetComponentByClass(USHealthComponent::StaticClass()));
			if(HealthComp && HealthComp->GetHealthValue() > 0.0f)
			{
				//UE_LOG(LogTemp, Display, TEXT("%s Status: Alive"), *Player->GetName());
			}
			else
			{
				//UE_LOG(LogTemp, Display, TEXT("%s Status: Dead"), *Player->GetName());
				GameOver();
			}
		}
	}
}

void ASGameMode::GameOver()
{
	EndWave();

	NewWaveState(EWaveState::GameOver);
}

void ASGameMode::NewWaveState(EWaveState NewState)
{
	auto GS = GetGameState<ASGameStateBase>();
	if(GS)
	{
		GS->SetWaveState(NewState);
	}
}

//DOESNT WORK
void ASGameMode::RestartDeadPlayer()
{
	for (auto It = TActorIterator<ASCharacter>(GetWorld()); It; ++It)
	{
		const auto Player = Cast<ASCharacter>(*It);
		if (Player)
		{
			const auto HealthComp = Cast<USHealthComponent>(Player->GetComponentByClass(USHealthComponent::StaticClass()));
			if((HealthComp) && HealthComp->GetHealthValue() <= 0.0f)
			{
				const auto PC = Cast<APlayerController>(Player->GetController());
				if(PC)
				{
					RestartPlayer(PC);
					UE_LOG(LogTemp, Display, TEXT("Try to reset PC: %s"), *PC->GetName());
				}
			}
		}
	}
}
