// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/SCharacter.h"
#include "Weapons/SWeapon.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/SHealthComponent.h"
#include "GameFramework/PawnMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "Animation/AnimationAsset.h"
#include "Gameframework/Controller.h"
#include "Net/UnrealNetwork.h"

// Sets default values
ASCharacter::ASCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpringArmComp = CreateDefaultSubobject<USpringArmComponent>("SpringArmComp");
	SpringArmComp->SetupAttachment(GetRootComponent());
	SpringArmComp->bUsePawnControlRotation = true;

	CameraComp = CreateDefaultSubobject<UCameraComponent>("CameraComp");
	CameraComp->SetupAttachment(SpringArmComp);

	GetMovementComponent()->GetNavAgentPropertiesRef().bCanCrouch = true; // Need to be true for Crouch (crazy shit to remember)

	HealthComponent = CreateDefaultSubobject<USHealthComponent>("HealthComponent");
}

// Called when the game starts or when spawned
void ASCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	DefaultFOV = CameraComp->FieldOfView;

	HealthComponent->OnTakePointDamageHealthChanged.AddDynamic(this, &ASCharacter::OnHealthChangedToPointDamage);
	HealthComponent->OnTakeRadialDamageHealthChanged.AddDynamic(this, &ASCharacter::OnHealthChangedToRadialDamage);
	
	//NETWORK
	if (GetLocalRole() == ENetRole::ROLE_Authority)
	{
		SpawnWeapons();
	}
}

// Called every frame
void ASCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	float TargetFOV = WantsToZoom ? ZoomedFOV : DefaultFOV;
	float NewFOV = FMath::FInterpTo(CameraComp->FieldOfView, TargetFOV, DeltaTime, ZoomInterpSpeed);

	CameraComp->SetFieldOfView(NewFOV);
}

// Called to bind functionality to input
void ASCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &ASCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ASCharacter::MoveRight);
	PlayerInputComponent->BindAxis("LookUp", this, &ASCharacter::AddControllerPitchInput); // buildin function for mouse control
	PlayerInputComponent->BindAxis("LookRight", this, &ASCharacter::AddControllerYawInput); // buildin function for mouse control

	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &ASCharacter::BeginCrouch); // see definition
	PlayerInputComponent->BindAction("Crouch", IE_Released, this, &ASCharacter::EndCrouch); // see definition
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ASCharacter::Jump); // buildin function

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ASCharacter::StartFire);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &ASCharacter::EndFire);

	PlayerInputComponent->BindAction("Zoom", IE_Pressed, this, &ASCharacter::StartZoom);
	PlayerInputComponent->BindAction("Zoom", IE_Released, this, &ASCharacter::EndZoom);
}

void ASCharacter::MoveForward(float inValue)
{
	this->AddMovementInput(this->GetActorForwardVector() * inValue);
}

void ASCharacter::MoveRight(float inValue)
{
	this->AddMovementInput(this->GetActorRightVector() * inValue);
}

void ASCharacter::BeginCrouch()
{
	Crouch(); // buildin finction
}

void ASCharacter::EndCrouch()
{
	UnCrouch(); // buildin finction
}

void ASCharacter::SpawnWeapons()
{
	CurrentWeapon = GetWorld()->SpawnActor<ASWeapon>(WeaponClass);
	if (CurrentWeapon)
	{
		CurrentWeapon->SetOwner(this);
		AttachWeaponToSocket(CurrentWeapon, this->GetMesh(), WeaponEquipSocketName);
	}
}

void ASCharacter::AttachWeaponToSocket(ASWeapon* Weapon, USceneComponent* SceneComponent, FName& SocketName)
{
	if (!Weapon || !SceneComponent) return;
	FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, false);
	Weapon->AttachToComponent(SceneComponent, AttachmentRules, SocketName);
}

void ASCharacter::StartFire()
{
	CurrentWeapon->CanFire = true;
	CurrentWeapon->MakeShot();
}

void ASCharacter::EndFire()
{
	CurrentWeapon->CanFire = false;
	CurrentWeapon->MakeShot();
}

void ASCharacter::StartZoom()
{
	WantsToZoom = true;
}
void ASCharacter::EndZoom()
{
	WantsToZoom = false;
}

void ASCharacter::OnRep_PlayerDied()
{
	PlayDeathAnimation();
}

void ASCharacter::PlayDeathAnimation()
{
	//plays death animations. need to fill the array in BP with animation assets
	if (DeathAnimations.Num() > 0)
	{
		const uint8 RandomNumber = FMath::RandRange(0, DeathAnimations.Num() - 1);
		GetMesh()->PlayAnimation(DeathAnimations[RandomNumber], false);
	}
	
}

void ASCharacter::OnHealthChangedToPointDamage(USHealthComponent* HealthComp, float Health)
{
	UE_LOG(LogTemp, Display, TEXT("HealthComponent PointDamage value: %f"), Health);

	if (Health <= 0.0f && !bDied)
	{
		bDied = true;

		GetMovementComponent()->StopMovementImmediately();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		CurrentWeapon->GetMeshComp()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		DisableAllInputsForPlayer();
		PlayDeathAnimation();
		
	}
}

void ASCharacter::OnHealthChangedToRadialDamage(USHealthComponent* HealthComp, float Health)
{
	if (Health == 0.0f && !bDied)
	{
		bDied = true;

		GetMovementComponent()->StopMovementImmediately();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		CurrentWeapon->GetMeshComp()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		DisableAllInputsForPlayer();
		PlayDeathAnimation();
	}
}

void ASCharacter::DisableAllInputsForPlayer()
{
	const auto PlayerController = Cast<APlayerController>(GetController());
	if (PlayerController)
	{
		EndFire();
		DisableInput(PlayerController);
	}
}

//NETWORK. For setup CurrentWeapon variable and others if needed; Replicate this variable on the Server and replicate it to the client
void ASCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	//MACRO works with #include "Net/UnrealNetwork.h"
	DOREPLIFETIME(ASCharacter, CurrentWeapon);
	DOREPLIFETIME(ASCharacter, bDied);
}



