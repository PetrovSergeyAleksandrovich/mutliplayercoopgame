// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SHealthComponent.generated.h"

class USHealthComponent;

//On Health Changed Event
//Your Own Delegate with parameters to get. First parameter is th TypeName of the DELEGATE to Call !
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnTakePointDamageSignature, USHealthComponent*, HelthComp, float, Health); // from function params
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnTakeRadialDamageSignature, USHealthComponent*, HelthComp, float, Health); // from function params


UCLASS( ClassGroup=(CoopGame), meta=(BlueprintSpawnableComponent) )
class COOPGAME_API USHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USHealthComponent();

	UFUNCTION(BlueprintCallable, Category = " HealthComponent")
	void Heal(float HealAmount);

	float GetHealthValue() const{return Health;}

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(ReplicatedUsing = OnRep_Health, EditDefaultsOnly, BlueprintReadWrite, Category = "HealthComponent")
	float Health = 100.0f;

	bool bIsDead = false;

	UFUNCTION()
	void OnRep_Health();

	float DefaultHealth = 100.0f; // for clamping usage

	UFUNCTION()
	void HandleTakePointDamage(AActor* DamagedActor, 
							   float Damage, 
							   class AController* InstigatedBy, 
							   FVector HitLocation, 
							   class UPrimitiveComponent* FHitComponent, 
							   FName BoneName, 
							   FVector ShotFromDirection, 
							   const class UDamageType* DamageType, 
							   AActor* DamageCauser);

	UFUNCTION()
	void HandleTakeRadialDamage(AActor* DamagedActor, 
								float Damage, 
								const class UDamageType* DamageType, 
								FVector Origin, 
								const FHitResult& HitInfo,
								class AController* InstigatedBy, 
								AActor* DamageCauser);

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

public:

	//Your own Delegate
	UPROPERTY(BlueprintAssignable, Category = "Events")
	FOnTakePointDamageSignature OnTakePointDamageHealthChanged; // This will be in Events in BP so node in EventGraph will be have passed data into Broadcast information parameters

	UPROPERTY(BlueprintAssignable, Category = "Events")
	FOnTakeRadialDamageSignature OnTakeRadialDamageHealthChanged; // This will be in Events in BP so node in EventGraph will be have passed data into Broadcast information parameters
};
