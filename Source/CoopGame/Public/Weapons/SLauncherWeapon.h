// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapons/SWeapon.h"
#include "SLauncherWeapon.generated.h"

class ASProjectile;

UCLASS()
class COOPGAME_API ASLauncherWeapon : public ASWeapon
{
	GENERATED_BODY()

public:

	virtual void Tick(float DeltaSeconds) override;

	virtual void Fire() override;

	virtual void MakeShot() override;

	bool CanLaunch = true;

protected:
	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	TSubclassOf<ASProjectile> Projectile;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	float LauncherReloadingTime = 3.0f;

private:

	FTimerHandle HandleFireRateLauncherTimer;
	void SetLauncherReadyToShot();

};

