// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SWeapon.generated.h"

class USkeletalMeshComponent;
class UDamageType;
class UParticleSystem;
class UCameraShakeBase;
class USoundCue;
class UPhysicalMaterial;
class UStaticMeshComponent;
class ASProjectile;

//Contains information of a single hitscan weapon linetrace
USTRUCT(BlueprintType)
struct FHitScanTrace
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	UPhysicalMaterial* PhysMat;  

	UPROPERTY()
	FVector_NetQuantize TraceTo;
};


USTRUCT(BlueprintType)
struct FImpactData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
	UParticleSystem* ParticleEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
	USoundCue* Sound;
};

UCLASS()
class COOPGAME_API ASWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASWeapon();

	virtual void Tick(float DeltaSeconds) override;

	virtual void Fire();

	void PlayFireEffects();

	virtual void PlayCameraShake();

	virtual void MakeShot();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerFire();
	void ServerFire_Implementation();
	bool ServerFire_Validate();

	bool CanFire = true;

protected:
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USkeletalMeshComponent* MeshComp;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	TSubclassOf<ASProjectile> MeshBulletEjected;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	FName  MuzzleSocketName = "MuzzleFlash";

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	FName  AmmoEjectSocketName = "AmmoEject";

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	FName  TracerTargetName = "Tracer";

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	TSubclassOf<UDamageType> DamageTypeClass;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	UParticleSystem* MuzzleEffect;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	UParticleSystem* ImpactEffect;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	UParticleSystem* TracerEffect;
	
	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	float FireRate = 0.2f;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	float BulletSpread = 2.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	float DamageValue = 10.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	TSubclassOf<UCameraShakeBase> FireCameraShake;

	//Hit Effects
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
	FImpactData DefaultImpactData;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
	TMap<UPhysicalMaterial*, FImpactData> ImpactDataMap;

	//HeadShot material
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "HeadShotParams")
	UPhysicalMaterial* HeadShotPhysMat;

	FTimerHandle HandleFireRateTimer;

	//Play Hit Effects
	void PlayImpactFX(const UPhysicalMaterial* PhysMat, const FVector ImpactPoint);

	void PlayAmmoEjects();

	//NETWORK
	UPROPERTY(ReplicatedUsing = OnRep_HitScanTrace) 
	FHitScanTrace HitScanTrace;

	//NETWORK
	UFUNCTION()
	void OnRep_HitScanTrace();//Use this function when variable HitScanTrace which listed above is updated

private:

	FTimerHandle EjectedAmmoTimerBeforeDestroy;

public:

	USkeletalMeshComponent* GetMeshComp() { return MeshComp; }


};
