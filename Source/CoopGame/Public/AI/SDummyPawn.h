// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Components/SHealthComponent.h"
#include "SDummyPawn.generated.h"

class USHealthComponent;
class UStaticMeshComponent;

UCLASS()
class COOPGAME_API ASDummyPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ASDummyPawn();

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleDefaultsOnly, Category = "Components")
	UStaticMeshComponent* MeshComp;

	UPROPERTY(VisibleDefaultsOnly, Category = "Components")
	USHealthComponent* HealthComponent;

	UFUNCTION()
	void OnHealthChangedToPointDamage(USHealthComponent* HealthComp, float Damage);

};
