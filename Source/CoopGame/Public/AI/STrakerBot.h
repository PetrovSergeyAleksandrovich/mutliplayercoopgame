// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Components/SHealthComponent.h"
#include "Components/SphereComponent.h"
#include "STrakerBot.generated.h"

class UStaticMeshComponent;
class USHealthComponent;
class USphereComponent;
class USoundBase;

UCLASS()
class COOPGAME_API ASTrakerBot : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ASTrakerBot();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleDefaultsOnly, Category = "Components")
	UStaticMeshComponent* MeshComp;

	UPROPERTY(VisibleDefaultsOnly, Category = "Components")
	USHealthComponent* HealthComponent;

	UPROPERTY(VisibleDefaultsOnly, Category = "Components")
	USphereComponent* SphereCollisionComp;

	FVector GetNextPathPoint();

	FVector NextPathPoint;

	UPROPERTY(EditDefaultsOnly, Category = "TrkerBot")
	float MovementForce = 1000.0f;

	UPROPERTY(EditDefaultsOnly, Category = "TrkerBot")
	bool bVelocityChange = true;

	UPROPERTY(EditDefaultsOnly, Category = "TrkerBot")
	float RequiredDistanceToTarget = 50.0f;

	//DynamicMaterialToPulseOnDamage
	UMaterialInstanceDynamic* MatInst;

	UPROPERTY(EditDefaultsOnly, Category = "VFX")
	UParticleSystem* ExplosionFX = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "VFX")
	USoundBase* ExplosionSound = nullptr;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void OnHealthChangedToPointDamage(USHealthComponent* HealthComp, float Health);

	UFUNCTION()
	void OnHealthChangedToRadialDamage(USHealthComponent* HealthComp, float Damage);

	UFUNCTION()
	void SelfDestructFX() const;

	UFUNCTION()
	void TriggeredExplosion(UPrimitiveComponent* OverlappedComponent,
							AActor* OtherActor,
							UPrimitiveComponent* OtherComp,
							int32 OtherBodyIndex,
							bool bFromSweep,
							const FHitResult& SweepResult);

	UFUNCTION(BlueprintCallable)
	void ExplodeBot();
};
