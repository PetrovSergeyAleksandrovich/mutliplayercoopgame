// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SHealthComponent.h"
#include "SDummyActor.generated.h"

class USHealthComponent;
class UStaticMeshComponent;

UCLASS()
class COOPGAME_API ASDummyActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASDummyActor();

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleDefaultsOnly, Category = "Components")
	UStaticMeshComponent* MeshComp;

	UPROPERTY(VisibleDefaultsOnly, Category = "Components")
	USHealthComponent* HealthComponent;

public:

	UFUNCTION()
	void OnHealthChangedToPointDamage(USHealthComponent* HealthComp, float Damage);

};
