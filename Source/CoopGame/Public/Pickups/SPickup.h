// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "Components/DecalComponent.h"
#include "GameFramework/Actor.h"
#include "Pickups/SPowerup.h"
#include "SPickup.generated.h"

class USphereComponent;
class UDecalComponent;
class ASPowerup;

UCLASS()
class COOPGAME_API ASPickup : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASPickup();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	USphereComponent* SphereComp;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	UDecalComponent* DecalComp;

	UPROPERTY(EditDefaultsOnly, Category = "PickupActor")
	TSubclassOf<ASPowerup> PowerUpPickup;

	ASPowerup* PowerupInst = nullptr;

	void Respawn();

	UPROPERTY(EditDefaultsOnly, Category = "PickupActor")
	float CoolDownDuration = 0.0f;

	FTimerHandle TimerHandle_RespwnTimer;

public:

	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
};
