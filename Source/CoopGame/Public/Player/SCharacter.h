// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Weapons/SWeapon.h"
#include "CoopGame/InfoHeader.h"
#include "SCharacter.generated.h"

class UCameraShakeBase;

UCLASS()
class COOPGAME_API ASCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ASCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//Movement inputs
	void MoveForward(float inValue);
	void MoveRight(float inValue);
	void BeginCrouch();
	void EndCrouch();
	void StartZoom();
	void EndZoom();

	//Camera
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	class UCameraComponent* CameraComp;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	class USpringArmComponent* SpringArmComp;

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	class USHealthComponent* HealthComponent;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	FName  WeaponEquipSocketName = "WeaponSocket";

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	TSubclassOf<ASWeapon> WeaponClass;

	//NETWORK. Replicated is added
	UPROPERTY(BlueprintReadOnly, Replicated, Category = "Components")
	class ASWeapon* CurrentWeapon;

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;

	bool WantsToZoom = false;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	float ZoomedFOV = 50.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon", meta = (ClampMin = 0.0 , ClampMax = 100.0))
	float ZoomInterpSpeed = 20.0f;

	float DefaultFOV = 90.0f;

	UFUNCTION()
	void OnHealthChangedToPointDamage(USHealthComponent* HealthComp, float Health); 

	UFUNCTION()
	void OnHealthChangedToRadialDamage(USHealthComponent* HealthComp, float Damage);

	UPROPERTY(EditDefaultsOnly, Category = "Animation")
	TArray<class UAnimationAsset*> DeathAnimations;

	/*Pawn died previously*/ //NETWORK
	UPROPERTY(Replicated, ReplicatedUsing = OnRep_PlayerDied, BlueprintReadOnly, Category = "Player")
	bool bDied = false;

	UFUNCTION()//NETWORKING
	void OnRep_PlayerDied();

	void PlayDeathAnimation();

	void DisableAllInputsForPlayer();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:

	void SpawnWeapons(); // Call SpawnWeapon at beginplay
	void AttachWeaponToSocket(ASWeapon* Weapon, USceneComponent* SceneComponent, FName& SocketName);
	void StartFire();
	void EndFire();


};
