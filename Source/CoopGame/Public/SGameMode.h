// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SGameStateBase.h"
#include "GameFramework/GameModeBase.h"
#include "SGameMode.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnActorKilled, AActor*, VictimActor, AActor*, KillerActor, AController*, KillerController);

UCLASS()
class COOPGAME_API ASGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:

	ASGameMode();

protected:

	FTimerHandle TimerHandle_BotSpawner;
	FTimerHandle TimerHandle_NextWaveStart;

	int32 AmountOfBotsToSpawn = 1;

	int32 WaveCount = 1;

	UPROPERTY(EditDefaultsOnly, Category = "GameMode")
	float TimeBetweenWaves = 15.0f;

	UPROPERTY(EditDefaultsOnly, Category = "GameMode")
	float TimeBetweenSpawningBots = 5.0f;

	bool WaveSpawnFinished = false;

protected:

	void SpawnBotTimerElapsed();

	//Hook for BP to spawn a single bot
	UFUNCTION(BlueprintImplementableEvent, Category = "GameMode")
	void SpawnNewBot();

	void StartWave();

	void EndWave();

	/**
	 * Set timer for next spawn wave
	 */
	void PrepareForNextWave();

	void CheckWaveState();

	void CheckAnyPlayerAlive();

	void NewWaveState(EWaveState NewState);

	void RestartDeadPlayer();

public:

	virtual void StartPlay() override;

	virtual void Tick(float DeltaSeconds) override;

	void GameOver();

	UPROPERTY(BlueprintAssignable, Category = "GameMode")
	FOnActorKilled OnActorKilled;
};
